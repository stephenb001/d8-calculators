<?php

namespace Drupal\cmme_calculators\Traits;

use Drupal\Core\Form\FormStateInterface;

trait CmmeFormFieldValidateTrait {

  /**
   * {@inheritdoc}
   */
  public function test() {
    return 'this is a test';
  }


  /**
   * Check that the balance is valid, either int or float
   *
   * @param type $element
   * @param type $form_state
   * @param type $form
   */
  public function cmme_bits_valid_balance($element, FormStateInterface $form_state, $form) {
    $value = $element['#value'];
    if ((!is_numeric($value))) {
      $form_state->setError($element, t('Please enter a valid balance.'));
    }
  }

  /**
   * Check that the balance is valid, either int or float
   *
   * @param type $element
   * @param type $form_state
   * @param type $form
   */
  public function cmme_bits_valid_interest($element, FormStateInterface $form_state, $form) {
    $value = $element['#value'];
    $value = preg_replace('/\s+/', '', $value);
    if ((!is_numeric($value))) {
      $form_state->setError($element, t('Please enter a valid interest rate'));
    }
  }

  /**
   * Custom valid email check for CMME
   *
   * @param type $element
   * @param type $form_state
   * @param type $form
   */
  function cmme_bits_email_validate($element, FormStateInterface $form_state, $form) {
    $value = $element['#value'];
    // @todo - replace the depreciated function valid_email_address
    if (!valid_email_address($value)) {
      $form_state->setError($element, t('Please enter a valid email address.'));
    }
  }

  function is_numerical_val($element, FormStateInterface $form_state, $form) {
    if (!is_numeric($element['#value'])) {
      $form_state->setError($element, t('Please enter a numerical value'));
    }
  }

}
