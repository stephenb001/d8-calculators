<?php
/**
 * @file
 * Contains \Drupal\cmme_calculators\Form\WorkForm.
 */
namespace Drupal\cmme_calculators\Form;

use Drupal\cmme_calculators\Traits\CmmeFormFieldValidateTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class MortgageRepaymentsForm extends FormBase {

  use CmmeFormFieldValidateTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mortgage_repayments_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['#attributes'] = [
      'class' => [
        'custom-calculator-form'
      ]
    ];

    $form['looking_to_borrow'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Looking to borrow'),
      '#placeholder' => $this->t("Enter how much you'd like to borrow"),
      '#required' => TRUE,
      '#element_validate' => [[get_class($this), 'cmme_bits_valid_balance']],
      '#default_value' => ((isset($_GET['amount']) && (!empty($_GET['amount']))) ? $_GET['amount'] : ''),
    ];

    $form['rate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rate of interest'),
      '#placeholder' => $this->t('Enter a rate of interest'),
      '#required' => TRUE,
      '#element_validate' => [[get_class($this), 'cmme_bits_valid_interest']],
      '#default_value' => ((isset($_GET['rate']) && (!empty($_GET['rate']))) ? $_GET['rate'] : ''),
    ];

    $form['length_of_mortgage'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Length of mortgage'),
      '#placeholder' => $this->t('Enter length in years'),
      '#required' => TRUE,
    ];

    // Now the introducer stuff.
    if (isset($_COOKIE['mortgage_keeper_ID'])) {
      $value = $_COOKIE['mortgage_keeper_ID'];
    }
    else {
      $value = 112;
    }

    $form['mkIntroducerID'] = [
      '#type' => 'hidden',
      '#value' => $value,
    ];


    $form['calculate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate now'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Save all the valid data into session for next page,
    // Also allows you to revist the page.
    $values = [
      'looking_to_borrow',
      'rate',
      'length_of_mortgage',
      'email_address',
      'phone_number',
      'mkIntroducerID'
    ];

    foreach ($form_state->getValues() as $form_value_k => $form_value_v) {
      if (in_array($form_value_k, $values)) {
        $_SESSION['monthly_payments'][$form_value_k] = $form_value_v;
      }
    }

    $txtInterest = $_SESSION['monthly_payments']['rate'];
    $txtLength = $_SESSION['monthly_payments']['length_of_mortgage'];
    $txtBorrow = $_SESSION['monthly_payments']['looking_to_borrow'];

    $I = $txtInterest / 12;
    $X = 1 / (1 + $I / 100);
    $N = $txtLength * 12;
    $L = $txtBorrow;
    $P1 = 0;
    $P2 = $txtBorrow;
    $repayment = (($L - $P1 * pow($X, $N)) * ($X - 1) / (pow($X, $N + 1) - $X));
    $interest = (($L - $P2 * pow($X, $N)) * ($X - 1) / (pow($X, $N + 1) - $X));

    $interest = $txtBorrow * $txtInterest / 1200;

    $_SESSION['monthly_payments']['result_repayment'] = $repayment;
    $_SESSION['monthly_payments']['result_interest'] = $interest;

    // Save results into the DB
    // @todo - Do we need to save this? Not collecting email addresses so no point?
//    $rid = db_insert('payments_results')
//      ->fields(array(
//        'ts' => REQUEST_TIME,
//        'borrow_amount' => $form_state['input']['looking_to_borrow'],
//        'interest' => $form_state['input']['rate'],
//        'years' => $form_state['input']['length_of_mortgage'],
//        'email_address' => $form_state['input']['email_address'],
//        'telephone_number' => $form_state['input']['phone_number']
//      ))
//      ->execute();


    /** @var Form ID that is used to track the form that created the lead $formID */
    $uri = \Drupal::request()->getRequestUri();
    $formID = $_SERVER['HTTP_HOST'] . $uri . $form_state->getBuildInfo()['form_id'];

    $_SESSION['oyster_calculator_type'] = array(
      'form_id' => 'calculator_payments_calculator',
      'crm_request_id' => 'payments-calculator',
      'crm_form_id' => $formID,
      'mail_key' => 'payments',
      'data' => $_SESSION['monthly_payments']
    );

//    // Go to results page (don't use drupal_goto as it kills other submission handlers)
//    $form_state['redirect'] = ['mortgage-calculators/mortgage-payments-calculator/your-results'];

    drupal_set_message($this->t('@emp_name ,Your application is being submitted!', array('@emp_name' => $form_state->getValue('employee_name'))));

  }
}