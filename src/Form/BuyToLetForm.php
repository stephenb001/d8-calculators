<?php
/**
 * @file
 * Contains \Drupal\cmme_calculators\Form\WorkForm.
 */
namespace Drupal\cmme_calculators\Form;

use Drupal\cmme_calculators\Traits\CmmeFormFieldValidateTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class BuyToLetForm extends FormBase {

  use CmmeFormFieldValidateTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'buy_to_let_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['#attributes'] = [
      'class' => [
        'custom-calculator-form'
      ]
    ];

    $form['#submit'][] = 'calculator_buy_to_let_submit';
    $form['monthly_rent'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your monthly rent?'),
      '#placeholder' => $this->t('Enter your current monthly rent'),
      '#required' => TRUE,
      '#element_validate' => [[get_class($this), 'cmme_bits_valid_balance']],
    ];

    $form['calculate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate now'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save all the valid data into session for next page,
    // Also allows you to revist the page.
    $values = [
      'monthly_rent',
      'email_address',
      'phone_number'
    ];

    foreach ($form_state->getValues() as $form_value_k => $form_value_v) {
      if (in_array($form_value_k, $values)) {
        $_SESSION['buy_to_let'][$form_value_k] = $form_value_v;
      }
    }

    /** @var Form ID that is used to track the form that created the lead $formID */
    $uri = \Drupal::request()->getRequestUri();
    $formID = $_SERVER['HTTP_HOST'] . $uri . $form_state->getBuildInfo()['form_id'];

    $_SESSION['oyster_calculator_type'] = array(
      'form_id' => 'calculator_buy_to_let',
      'crm_request_id' => 'buytolet-calculator',
      'crm_form_id' => $formID,
      'mail_key' => 'buytolet',
      'data' => $_SESSION['buy_to_let']
    );

    // Go to results page (don't use drupal_goto as it kills other submission handlers)
//    $form_state['redirect'] = ['mortgage-calculators/buy-to-let-calculator/your-results'];
    drupal_set_message($this->t('@emp_name ,Your application is being submitted!', array('@emp_name' => $form_state->getValue('employee_name'))));

  }
}