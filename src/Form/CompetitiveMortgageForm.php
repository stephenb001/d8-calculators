<?php
/**
 * @file
 * Contains \Drupal\cmme_calculators\Form\WorkForm.
 */
namespace Drupal\cmme_calculators\Form;

use Drupal\cmme_calculators\Traits\CmmeFormFieldValidateTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class CompetitiveMortgageForm extends FormBase {

  use CmmeFormFieldValidateTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'competitive_mortgage_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['#attributes'] = [
      'class' => [
        'custom-calculator-form'
      ]
    ];

    //$form['#prefix'] = '<p>' . variable_get('stage1_text', 'For further information about our best rates, please enter your details below') . '</p>';

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => t('Your name'),
      '#placeholder' => $this->t('Enter your full name'),
      '#attributes' => ['autocomplete' => 'name'],
      '#required' => TRUE
    ];

    $form['email_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your email address'),
      '#placeholder' => $this->t('Enter your email address'),
      '#attributes' => ['autocomplete' => 'email'],
      '#required' => TRUE,
      '#default_value' => (isset($_SESSION['email_address']) ? $_SESSION['email_address'] : ''),
      '#element_validate' => [[get_class($this), 'cmme_bits_email_validate']],
    ];

    $form['phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your telephone number'),
      '#placeholder' => $this->t('Enter your telephone number'),
      '#attributes' => ['autocomplete' => 'tel'],
      '#required' => FALSE,
      '#default_value' => (isset($_SESSION['phone_number']) ? $_SESSION['phone_number'] : ''),
    ];

    $form['calculate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Compare rates'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = [
      'email_address',
      'phone_number',
      'name',
    ];

    foreach ($form_state->getValues() as $form_value_k => $form_value_v) {
      if (in_array($form_value_k, $values)) {
        $_SESSION['remortgage_savings'][$form_value_k] = $form_value_v;
      }
    }

    // @var Form ID that is used to track the form that created the lead $formID.
    $uri = \Drupal::request()->getRequestUri();
    $formID = $_SERVER['HTTP_HOST'] . $uri . $form_state->getBuildInfo()['form_id'];

    // Send record to CRM system.
    // @todo - need to implement this function: crm_send....
//    $response = crm_send('remortgage-calculator', $_SESSION['remortgage_savings'], $formID);

//    // Go to results page (don't use drupal_goto as it kills other submission handlers)
//    $form_state['redirect'] = [
//      'mortgage-calculators/competitive-mortgage-calculator',
//      [
//        'query' => [
//          'name' => $form_state['values']['name'],
//          'email' => $form_state['values']['email_address'],
//          'phone' => $form_state['values']['phone_number']
//        ]
//      ]
//    ];
    drupal_set_message($this->t('@emp_name ,Your application is being submitted!', ['@emp_name' => $form_state->getValue('employee_name')]));

  }
}