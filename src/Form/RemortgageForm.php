<?php
/**
 * @file
 * Contains \Drupal\cmme_calculators\Form\WorkForm.
 */
namespace Drupal\cmme_calculators\Form;

use Drupal\cmme_calculators\Traits\CmmeFormFieldValidateTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class RemortgageForm extends FormBase {

  use CmmeFormFieldValidateTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'remortgage_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['#attributes'] = [
      'class' => [
        'custom-calculator-form'
      ]
    ];

    $form['remaining_loan_balance'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remaining loan balance'),
      '#placeholder' => $this->t('Enter remaining loan balance'),
      '#required' => TRUE,
      '#element_validate' => [[get_class($this), 'cmme_bits_valid_balance']],

    ];

    $form['remaining_mortgage_term'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remaining mortgage term'),
      '#placeholder' => $this->t('Enter remaining mortgage term in years'),
      '#required' => TRUE,
    ];

    $form['current_rate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Current rate of interest'),
      '#placeholder' => $this->t('Enter current rate of interest'),
      '#required' => TRUE,
      '#element_validate' => [[get_class($this), 'cmme_bits_valid_interest']],

    ];

    $form['new_rate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('New rate of interest'),
      '#placeholder' => $this->t('Enter new rate of interest'),
      '#required' => TRUE,
      '#element_validate' => [[get_class($this), 'cmme_bits_valid_interest']],

    ];

    $form['calculate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate now'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save all the valid data into session for next page,
    // Also allows you to revist the page.
    $values = [
      'remaining_loan_balance',
      'remaining_mortgage_term',
      'current_rate',
      'new_rate',
      'email_address',
      'phone_number',
    ];

    foreach ($form_state->getValues() as $form_value_k => $form_value_v) {
      if (in_array($form_value_k, $values)) {
        $_SESSION['remortgage_savings'][$form_value_k] = $form_value_v;
      }
    }

//    // Save results into the DB
//    $rid = db_insert('savings_results')
//      ->fields(array(
//        'ts' => REQUEST_TIME,
//        'loan' => $form_state['input']['remaining_loan_balance'],
//        'term' => $form_state['input']['remaining_mortgage_term'],
//        'current_interest' => $form_state['input']['current_rate'],
//        'new_interest' => $form_state['input']['new_rate'],
//        'email_address' => $form_state['input']['email_address'],
//        'telephone' => $form_state['input']['phone_number']
//      ))
//      ->execute();

    /** @var Form ID that is used to track the form that created the lead $formID */
    $uri = \Drupal::request()->getRequestUri();
    $formID = $_SERVER['HTTP_HOST'] . $uri . $form_state->getBuildInfo()['form_id'];

    // @todo - not sure if we need this session variable.
    $_SESSION['oyster_calculator_type'] = [
      'form_id' => 'calculator_remortgage_savings',
      'crm_request_id' => 'remortgage-calculator',
      'crm_form_id' => $formID,
      'mail_key' => 'remortgage',
      'data' => $_SESSION['remortgage_savings'],
    ];

    // Store the submission page URI so it can be tracked back in GA
    $submission_uri = $uri;

//    // Go to results page (don't use drupal_goto as it kills other submission handlers)
//    $form_state['redirect'] = [
//      'mortgage-calculators/remortgage-savings-calculator/your-results',
//      [
//        'query' => [
//          'submission_uri' => drupal_encode_path($submission_uri)
//        ]
//      ]
//    ];
    drupal_set_message($this->t('@emp_name ,Your application is being submitted!', array('@emp_name' => $form_state->getValue('employee_name'))));

  }
}