<?php
/**
 * @file
 * Contains \Drupal\cmme_calculators\Form\WorkForm.
 */
namespace Drupal\cmme_calculators\Form;

use Drupal\cmme_calculators\Traits\CmmeFormFieldValidateTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class BorrowForm extends FormBase {

  use CmmeFormFieldValidateTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'borrow_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['#attributes'] = [
      'class' => [
        'custom-calculator-form',
      ]
    ];

    $form['daily_rate'] = [
      '#type' => 'textfield',
      '#title' => t("Your daily rate?"),
      '#placeholder' => $this->t('Enter your daily rate'),
      '#element_validate' => [[get_class($this), 'is_numerical_val']],
      '#required' => TRUE,
    ];

    $form['calculate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Calculate now'),
    ];

    // Now the introducer stuff.
    if (isset($_COOKIE['mortgage_keeper_ID'])) {
      $value = $_COOKIE['mortgage_keeper_ID'];
    }
    else {
      $value = 112;
    }

    $form['mkIntroducerID'] = [
      '#type' => 'hidden',
      '#value' => $value,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save all the valid data into session for next page,
    // Also allows you to revist the page.
    $values = [('daily_rate', 'mkIntroducerID');
    foreach ($form_state['input'] as $form_value_k => $form_value_v) {
      if (in_array($form_value_k, $values)) {
        $_SESSION['calculator_borrow'][$form_value_k] = $form_value_v;
      }
    }
//
//    // daily rate x 5 x 48 x 4.5
//    $txtRate = $_SESSION['calculator_borrow']['daily_rate'];
//
//    // Calculate the daily rate
//    $annual = $txtRate * 5 * 48;
//    $borrow = $annual * 4.5;
//
//    $_SESSION['calculator_borrow']['borrow'] = $borrow;
//
//    // Save results into the DB
////    $rid = db_insert('borrow_results')
////      ->fields(array(
////        'ts' => REQUEST_TIME,
////        'daily_rate' => $form_state['input']['daily_rate']
////      ))
////      ->execute();
//
//    $formID = $_SERVER['HTTP_HOST'] . request_uri() . $form_state['build_info']['form_id'];
//
//    $_SESSION['oyster_calculator_type'] = array(
//      'form_id' => 'calculator_borrow_calculator',
//      'crm_request_id' => 'borrow-calculator',
//      'crm_form_id' => $formID,
//      'mail_key' => 'borrow',
//      'data' => $_SESSION['calculator_borrow']
//    );
//
//    // Go to results page (don't use drupal_goto as it kills other submission handlers)
//    $form_state['redirect'] = ['mortgage-calculators/how-much-can-you-borrow/your-results'];
    drupal_set_message($this->t('@emp_name ,Your application is being submitted!', array('@emp_name' => $form_state->getValue('employee_name'))));

  }
}