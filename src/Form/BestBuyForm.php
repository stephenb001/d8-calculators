<?php
/**
 * @file
 * Contains \Drupal\cmme_calculators\Form\WorkForm.
 */
namespace Drupal\cmme_calculators\Form;

use Drupal\cmme_calculators\Traits\CmmeFormFieldValidateTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class BestBuyForm extends FormBase {

  use CmmeFormFieldValidateTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'best_buy_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['#attributes'] = array(
      'class' => array(
        'custom-calculator-form'
      )
    );

    $form['remaining_loan_balance'] = [
      '#type' => 'textfield',
      '#title' => t('Current Mortgage Balance'),
      '#placeholder' => $this->t('Enter Current Mortgage Balance'),
      '#required' => TRUE,
      '#element_validate' => [[get_class($this), 'cmme_bits_valid_balance']],
    ];


    $form['current_rate'] = [
      '#type' => 'textfield',
      '#title' => t('Current Interest Rate'),
      '#placeholder' => $this->t('Enter current rate of interest'),
      '#required' => TRUE,
//      '#element_validate' => ['cmme_bits_valid_interest',
      '#element_validate' => [[get_class($this), 'cmme_bits_valid_interest']],
    ];


//    //get the get new rate
//    $result = db_query("SELECT entity_id FROM {field_data_field_use_in_calculator} WHERE field_use_in_calculator_value = 1 LIMIT 1");
//    $data = $result->fetchAll();
//    $best_rate = entity_load('rates', array($data[0]->entity_id));
//    $best_rate = $best_rate[$data[0]->entity_id]->field_rate[LANGUAGE_NONE][0]['value'];
$best_rate = '';
    $form['new_rate'] = [
      '#type' => 'hidden',
      '#title' => t('New rate of interest'),
      '#placeholder' => $this->t('Enter new rate of interest'),
      '#value' => $best_rate,
     // '#element_validate' => array('cmme_bits_valid_interest'),
      '#element_validate' => [[get_class($this), 'cmme_bits_valid_interest']],
    ];

    $form['email_address'] = [
      '#type' => 'textfield',
      '#title' => t('Email address'),
      '#placeholder' => t('Enter your email address'),
      '#required' => TRUE,
      '#default_value' => (isset($_SESSION['email_address']) ? $_SESSION['email_address'] : ''),
      //'#element_validate' => array('cmme_bits_email_validate'),
      '#element_validate' => [[get_class($this), 'cmme_bits_email_validate']],
    ];

    $form['phone_number'] = [
      '#type' => 'textfield',
      '#title' => t('Telephone number'),
      '#placeholder' => t('Enter your telephone number'),
      '#required' => FALSE,
      '#default_value' => (isset($_SESSION['phone_number']) ? $_SESSION['phone_number'] : ''),
    ];

    $form['calculate'] = [
      '#type' => 'submit',
      '#value' => t('Calculate now'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
//    // Save all the valid data into session for next page,
//    // Also allows you to revist the page.
//
//
//    $values = array(
//      'remaining_loan_balance',
//      'remaining_mortgage_term',
//      'current_rate',
//      'new_rate',
//      'email_address',
//      'phone_number'
//    );
//    foreach ($form_state['input'] as $form_value_k => $form_value_v) {
//      if (in_array($form_value_k, $values)) {
//        $_SESSION['remortgage_savings'][$form_value_k] = $form_value_v;
//      }
//    }
//
//    // Save results into the DB
//    $rid = db_insert('savings_results')
//      ->fields(array(
//        'ts' => REQUEST_TIME,
//        'loan' => $form_state['input']['remaining_loan_balance'],
//        'term' => $form_state['input']['remaining_mortgage_term'],
//        'current_interest' => $form_state['input']['current_rate'],
//        'new_interest' => $form_state['input']['new_rate'],
//        'email_address' => $form_state['input']['email_address'],
//        'telephone' => $form_state['input']['phone_number']
//      ))
//      ->execute();
//
//    /** @var Form ID that is used to track the form that created the lead $formID */
//    $formID = $_SERVER['HTTP_HOST'] . request_uri() . $form_state['build_info']['form_id'];
//    // Send record to CRM system.
//    // @todo - mpleent the function crm_send.
//    //$response = crm_send('remortgage-calculator', $_SESSION['remortgage_savings'], $formID);
//
//    //email the user
//    $mail_r = drupal_mail('calculator', 'remortgagebest', $form_state['input']['email_address'], LANGUAGE_NONE, $params, 'clientservices@cmme.co.uk');
//
//
//    // Go to results page (don't use drupal_goto as it kills other submission handlers)
//    $form_state['redirect'] = ['mortgage-calculators/bestbuy-savings-calculator/your-results'];
    drupal_set_message($this->t('@emp_name ,Your application is being submitted!', array('@emp_name' => $form_state->getValue('employee_name'))));

  }
}