<?php
/**
 * @file
 * Contains \Drupal\article\Plugin\Block\ArticleBlock.
 */
namespace Drupal\cmme_calculators\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
/**
 * Provides a 'best buy' block.
 *
 * @Block(
 *   id = "best_buy_block",
 *   admin_label = @Translation("Best buy block"),
 *   category = @Translation("Mortgage calculators block")
 * )
 */
class BestBuyBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\cmme_calculators\Form\BestBuyForm');
    return $form;
  }
}
