<?php
/**
 * @file
 * Contains \Drupal\article\Plugin\Block\ArticleBlock.
 */
namespace Drupal\cmme_calculators\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
/**
 * Provides a 'buy to let' block.
 *
 * @Block(
 *   id = "buy_to_let_block",
 *   admin_label = @Translation("Mortgages buy to let block"),
 *   category = @Translation("Mortgage calculators block")
 * )
 */
class BuyToLetBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\cmme_calculators\Form\BuyToLetForm');
    return $form;
  }
}