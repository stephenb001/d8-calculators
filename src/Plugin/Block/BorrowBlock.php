<?php
/**
 * @file
 * Contains \Drupal\article\Plugin\Block\ArticleBlock.
 */
namespace Drupal\cmme_calculators\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
/**
 * Provides a 'buy to let' block.
 *
 * @Block(
 *   id = "borrow_block",
 *   admin_label = @Translation("Borrow block"),
 *   category = @Translation("Mortgage calculators block")
 * )
 */
class BorrowBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\cmme_calculators\Form\BorrowForm');
    return $form;
  }
}
