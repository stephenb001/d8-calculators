<?php
/**
 * @file
 * Contains \Drupal\article\Plugin\Block\ArticleBlock.
 */
namespace Drupal\cmme_calculators\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
/**
 * Provides a 'Remortgage' block.
 *
 * @Block(
 *   id = "remortgage_block",
 *   admin_label = @Translation("Remortgage block"),
 *   category = @Translation("Mortgage calculators block")
 * )
 */
class RemortgageBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\cmme_calculators\Form\RemortgageForm');
    return $form;
  }
}