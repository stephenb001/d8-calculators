<?php
/**
 * @file
 * Contains \Drupal\article\Plugin\Block\ArticleBlock.
 */
namespace Drupal\cmme_calculators\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
/**
 * Provides a 'article' block.
 *
 * @Block(
 *   id = "mortgage_repayments_block",
 *   admin_label = @Translation("Mortgages Repayments calculator block"),
 *   category = @Translation("Mortgage calculators block")
 * )
 */
class MortgageRepaymentBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\cmme_calculators\Form\MortgageRepaymentsForm');
    return $form;
  }
}